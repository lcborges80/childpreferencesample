package com.borges.childpreferencesample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private FrameLayout preferenceArea;
    private ImageView blueHeart;
    private int x;
    private int y;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Child Preference");
        this.preferenceArea = (FrameLayout) findViewById(R.id.preferenceArea);
        this.blueHeart = (ImageView) findViewById(R.id.blueHeart);
        this.blueHeart.setOnTouchListener(new BlueHeartTouchListener2());
    }

    private final class BlueHeartTouchListener2 implements View.OnTouchListener {

        public boolean onTouch(View view, MotionEvent event) {
            int rawX = (int) event.getRawX();
            int rawY = (int) event.getRawY();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {

                case MotionEvent.ACTION_DOWN:
                    FrameLayout.LayoutParams layoutParamsOnActionDown = (FrameLayout.LayoutParams) view.getLayoutParams();
                    x = rawX - layoutParamsOnActionDown.leftMargin;
                    y = rawY - layoutParamsOnActionDown.topMargin;
                    break;

                case MotionEvent.ACTION_UP:
                    break;

                case MotionEvent.ACTION_POINTER_DOWN:
                    break;

                case MotionEvent.ACTION_POINTER_UP:
                    break;

                case MotionEvent.ACTION_MOVE:
                    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
                    layoutParams.leftMargin = rawX - x;
                    layoutParams.topMargin = rawY - y;
                    view.setLayoutParams(layoutParams);
                    break;
            }
            preferenceArea.invalidate();
            return true;
        }
    }

}
